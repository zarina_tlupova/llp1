#include "data_iterator.h"


data_iterator* init_iterator(database* db, table* tb) {
    data_iterator* iter = malloc(sizeof (data_iterator));
    iter->tb = tb;
    iter->db = db;
    iter->curPage = tb->firstPage;
    iter->ptr = 0;
    iter->rowsReadOnPage = 0;
    return iter;
}

int32_t getOffsetToColumnData(data_iterator* iter, const char* columnName, columnType colType) {
    int32_t offsetToColumnData = sizeof(dataHeader);
    bool match = false;
    for (int i = 0; i < iter->tb->header->columnAmount; i++) {
        columnHeader currentColumn = iter->tb->header->columns[i];
        if (currentColumn.type == colType && strcmp(columnName, currentColumn.columnName) == 0) {
            match = true;
            break;
        }
        offsetToColumnData += currentColumn.size;
    }
    if (!match) return -1;
    return offsetToColumnData;
}

bool hasNext(data_iterator* iter) {
    return iter->ptr + iter->tb->header->oneRowSize <= DEFAULT_PAGE_SIZE - sizeof(tableHeader) - sizeof(pageHeader)
    && iter->rowsReadOnPage < iter->curPage->header->rowsAmount;
}

bool seekNext(data_iterator* iter) {
    dataHeader dHeader = {false};
    uint32_t nextPageNumber = iter->curPage->header->next_page_ordinal;
    while (hasNext(iter) || nextPageNumber != 0) {
        do {
            if (iter->rowsReadOnPage > 0) {
                iter->ptr += iter->tb->header->oneRowSize;
            }
            dHeader = getDataHeader(iter);
            iter->rowsReadOnPage += 1;
        } while (hasNext(iter) && !dHeader.valid);

        if (dHeader.valid) {
            return true;
        }

        if (iter->curPage != iter->tb->firstPage && iter->curPage != iter->tb->firstAvailableForWritePage) {
            close_page(iter->curPage);
        }
        if (nextPageNumber == 0) {
            break;
        }
        iter->curPage = read_page(iter->db, nextPageNumber);
        nextPageNumber = iter->curPage->header->next_page_ordinal;
        iter->rowsReadOnPage = 0;
        iter->ptr = 0;
    }
    return false;
}

bool seekNextWhere(data_iterator* iter, const char* colName, columnType colType, const void* val) {
    dataHeader dHeader = {false};

    int32_t colInt;
    char* colChar;
    bool colBool;
    double colFloat;
    uint32_t nextPageNumber = iter->curPage->header->next_page_ordinal;
    while (hasNext(iter) || nextPageNumber != 0) {
        do {
            if (iter->rowsReadOnPage > 0) {
                iter->ptr += iter->tb->header->oneRowSize;
            }
            dHeader = getDataHeader(iter);
            iter->rowsReadOnPage += 1;
        } while (hasNext(iter) && !dHeader.valid);

        if (dHeader.valid) {
            switch (colType) {
                case INTEGER:
                    getInteger(iter, colName, &colInt);
                    if (colInt == *((int32_t *) val)) {
                        return true;
                    }
                    continue;
                case STRING:
                    colChar = malloc(sizeof(char) * DEFAULT_STRING_LENGTH);
                    getString(iter, colName, &colChar);
                    if (strcmp(colChar, *((char**) val)) == 0) {
                        free(colChar);
                        return true;
                    }
                    free(colChar);
                    continue;
                case BOOLEAN:
                    getBool(iter, colName, &colBool);
                    if (colBool == *((bool *) val)) {
                        return true;
                    }
                    continue;
                case FLOAT:
                    getFloat(iter, colName, &colFloat);
                    if (colFloat == *((double *) val)) {
                        return true;
                    }
                    continue;
                default:
                    break;
            }
        }

        if (iter->curPage != iter->tb->firstPage && iter->curPage != iter->tb->firstAvailableForWritePage) {
            close_page(iter->curPage);
        }
        if (nextPageNumber == 0) {
           break;
        }
        iter->curPage = read_page(iter->db, nextPageNumber);
        nextPageNumber = iter->curPage->header->next_page_ordinal;
        iter->rowsReadOnPage = 0;
        iter->ptr = 0;
    }
    return false;
}

dataHeader getDataHeader(data_iterator* iter) {
    return *((dataHeader*)((char*) iter->curPage->bytes + iter->ptr));
}

bool getSome(data_iterator* iter, const char* columnName, void* dest) {
    for (int i = 0; i < iter->tb->header->columnAmount; i++) {
        columnHeader currentColumn = iter->tb->header->columns[i];
        if (strcmp(columnName, currentColumn.columnName) == 0) {
            switch (currentColumn.type) {
                case INTEGER:
                    return getInteger(iter, columnName, dest);
                case STRING:
                    return getString(iter, columnName, (char **) &dest);
                case BOOLEAN:
                    return getBool(iter, columnName, dest);
                case FLOAT:
                    return getFloat(iter, columnName, dest);
                default:
                    return false;
            }
        }
    }
    return false;
}

bool getInteger(data_iterator* iter, const char* columnName, int32_t* dest) {
    int32_t offsetToColumnData = getOffsetToColumnData(iter, columnName, INTEGER);
    if (offsetToColumnData == -1) return false;
    *dest = *((int32_t*)((char*) iter->curPage->bytes + iter->ptr + offsetToColumnData));
    return true;
}

bool getString(data_iterator* iter, const char* columnName, char** dest) {
    int32_t offsetToColumnData = getOffsetToColumnData(iter, columnName, STRING);
    if (offsetToColumnData == -1) return false;
    strcpy(*dest, (char*) iter->curPage->bytes + iter->ptr + offsetToColumnData);
    return true;
}

bool getBool(data_iterator* iter, const char* columnName, bool* dest) {
    int32_t offsetToColumnData = getOffsetToColumnData(iter, columnName, BOOLEAN);
    if (offsetToColumnData == -1) return false;
    *dest = *((bool*)((char*) iter->curPage->bytes + iter->ptr + offsetToColumnData));
    return true;
}

bool getFloat(data_iterator* iter, const char* columnName, double* dest) {
    int32_t offsetToColumnData = getOffsetToColumnData(iter, columnName, FLOAT);
    if (offsetToColumnData == -1) return false;
    *dest = *((double *)((char*) iter->curPage->bytes + iter->ptr + offsetToColumnData));
    return true;
}

uint16_t deleteWhere(database* db, table* tb, const char* colName, columnType colType, const void* val) {
    data_iterator* iter = init_iterator(db, tb);
    uint16_t removedObjects = 0;

    while (seekNextWhere(iter, colName, colType, val)) {
        page* pg = iter->curPage;
        void* pageBytes = pg->bytes;
        dataHeader header = getDataHeader(iter);
        header.valid = false;
        memcpy(pageBytes + iter->ptr, &header, sizeof(dataHeader));
        write_page_to_file(pg, db->file);
        removedObjects++;
    }
    free(iter);
    return removedObjects;
}

uint16_t updateWhere(database* db, table* tb,
                     const char* whereColName, columnType whereColType, const void* whereVal,
                     const char* updateColName, columnType updateColType, const void* updateVal) {
    data_iterator* iter = init_iterator(db, tb);
    uint16_t updatedObjects = 0;
    while (seekNextWhere(iter, whereColName, whereColType, whereVal)) {
        page* pg = iter->curPage;
        void* pageBytes = pg->bytes;
        int32_t columnDataOffset = getOffsetToColumnData(iter, updateColName, updateColType);
        columnHeader* c_header = get_col_header_by_name(iter->tb, updateColName);
        memcpy(pageBytes + iter->ptr + columnDataOffset, updateVal, c_header->size);
        write_page_to_file(pg, db->file);
        updatedObjects++;
    }
    free(iter);
    return updatedObjects;
}


void printJoinTable(database* db, table* tb1, table* tb2, const char* onColumnT1, const char* onColumnT2, columnType type) {

    data_iterator* iter1 = init_iterator(db,tb1);
    data_iterator* iter2 = init_iterator(db, tb2);
    while (seekNext(iter1)) {
        columnHeader* onColumnT1Header = get_col_header_by_name(tb1, onColumnT1);
        void* value = malloc(onColumnT1Header->size);
        getSome(iter1, onColumnT1, value);
        while (seekNextWhere(iter2, onColumnT2, type, value)) {
            for (int i = 0; i < iter1->tb->header->columnAmount; i++) {
                columnHeader cheader = iter1->tb->header->columns[i];
                void* curColumnValue = malloc(cheader.size);
                getSome(iter1, cheader.columnName, curColumnValue);
                switch (cheader.type) {
                    case INTEGER:
                        printf("\t %d \t", *((int32_t*) curColumnValue));
                        break;
                    case STRING:
                        printf("\t%s\t", (char*) curColumnValue);
                        break;
                    case BOOLEAN:
                        printf("\t%d\t", *((bool*) curColumnValue));
                        break;
                    case FLOAT:
                        printf("\t%f\t", *((float*) curColumnValue));
                        break;
                }
            }

            for (int i = 0; i < iter2->tb->header->columnAmount; i++) {
                columnHeader cheader = iter2->tb->header->columns[i];
                void* curColumnValue = malloc(cheader.size);
                getSome(iter2, cheader.columnName, curColumnValue);
                switch (cheader.type) {
                    case INTEGER:
                        printf("\t%d\t", *((int32_t*) curColumnValue));
                        break;
                    case STRING:
                        printf("\t%s\t", (char*) curColumnValue);
                        break;
                    case BOOLEAN:
                        printf("\t%d\t", *((bool*) curColumnValue));
                        break;
                    case FLOAT:
                        printf("\t%f\t", *((float*) curColumnValue));
                        break;
                }
            }

            printf("\n");
        }
        iter2 = init_iterator(db, tb2);
    }
    free(iter1);
    free(iter2);

}



